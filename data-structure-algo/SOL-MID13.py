# No 1
def mystery(x1):
    x2 = []
    for t1 in x1:
        x2.insert(0,t1)
    for t2 in x2:
        for idx in range(len(x1)):
            for t3 in x2:
                print(t2, t3, x2[idx])

#(a) Write down the output of calling mystery(['a','b'])'
    '''
    b b b
    b a b
    b b a
    b a a
    a b b
    a a b
    a b a
    a a a
    '''
#(b) If N represents the size of the input list x1, what is the time complexity of mystery above? Explain your answer!
    '''
    Time complexity would be O(n^3).
    '''
#(c) If mystery takes 1 second to process when the size of x1 is 100, how much time does it need to process input of size 10000?
    '''
    T(100) = 1
    T(10000) = (10000/100)^3 X 1
    T (10000) = (100/1)^3 = 10^2 x ^3 = 10^6
    '''

# No 2
''' Q: Accounts with a larger balance should appear before accounts with smaller balance
If two accounts have the same balance, they should appear in alphabetical ascending
order based on their name.
If two accounts have the same balance and name, they should appear in alphabetical
ascending order based on their id.'''

class Account:
    def __init__(self, i, n, b):
    self.id = i
    self.name = n
    self.balance = b

    def function(a,b):
        bigger = a
        if a.balance < b.balance:
            bigger == b
        elif a.balance == b.balance:
            if a.name > b.name:
                bigger = a
            elif a.id > b.id:
                bigger = a
            elif a.id == b.id:
                bigger = 0 

    if bigger == a:
        return -1       # a negative integer if a should appear BEFORE b
    elif bigger == b:
        return 1        # a positive integer if a should appear AFTER b
    else:
        return bigger   # zero if a and b are equal

# No 3
class Node:
    def __init__(self, d, n):
        self.data = d
        self.next = n
        
class Kyu:
    def __init__(self):
        self.front = None
        self.back = None

    def isEmpty(self):
     # returns a boolean stating whether the queue is empty or not
     return True if self.front is not None else False

    def makeEmpty(self): 
    # empied the queue
    self.front = None
    
    def dequeue(self): 
    # removes the item at the front of the queue and returns its value
    if self.front is not None:
        value = self.front.data
        self.front = self.front.next
        return value
    else: 
        return None

    def enqueue(self, x): 
    # adds a new node containing x to the back of the queue
    if self.front is not None:
        self.back.next = Node(x, None)
        self.back = self.back.next
    else:
        self.front = Node(x, None)

# No 4
''' Why binary search O(log n)? '''

    '''
    ANSWER: Because  you can divide log N times until you have everything divided. 
    Which means you have to divide log N ("do the binary search step") until you found your element.
    '''

# No 5
''' Combine listA & listB, expected output :
The list will be combined altogether, and be sorted alphabetically
'''

def combine(listA, listB):
    l = []
    while listA and listB:
        if listA[0] < listB[0]:
            l.append(listA.pop(0)) # remove the first item in listA, return to top, append to l (empty list)
        else:
            l.append(listB.pop(0)) # remove the first item in listB, return to top, append to l (empty list)
    return l + listA + listB       # return with merge lists that is already sorted

listA = ["aardvark", "dog", "gorilla", "zebra"]
listB = ["banana","cherry","durian"]
print (combine(listA,listB))
