from tkinter import *
from tkinter.colorchooser import askcolor
from tkinter.filedialog import asksaveasfilename

class DrawRubberShapes(object):
    # Construct the GUI
    def __init__(self):
        window = Tk()                               # Create a window 
        window.title("Simple Paint Application")    # Set a title       
        frame1 = Frame(window)                      # Create and add a frame to window 
        frame1.pack()    

        # Create a button for choosing color using color pallete
        """ This method will create an 'askcolor' button for user's desired colors """
        self.fillColor = StringVar()
        self.fillColor.set('LightSalmon')
        def colorCommand():
            (rgb,color) = askcolor()
            if color != None:
                self.fillColor.set(color)
                colorButton["bg"] = color        
        colorButton = Button(frame1, text = "Color",
            command=colorCommand, relief=RIDGE, bg = self.fillColor.get())
        colorButton.grid(row=1,column=1,columnspan=2)
        
        # Create radio buttons for geometrical shapes
        """ Radio buttons are used to choose preffered shapes """
        self.v1 = StringVar()
        rbRectangle = Radiobutton(frame1, text = "Rectangle", 
            variable = self.v1, value = 'R', 
            command = self.processRadiobutton)
        rbRectangle.select()                    # Default shape
        rbOval = Radiobutton(frame1, text = "Circle",
            variable = self.v1, value = 'C',
            command = self.processRadiobutton)
        rbLine = Radiobutton(frame1, text = "Line",
            variable = self.v1, value = 'L',
            command = self.processRadiobutton)
        rbPoly = Radiobutton(frame1, text = "Polygon",
            variable = self.v1, value = 'P',
            command = self.processRadiobutton)
        rbFree = Radiobutton(frame1, text = "Free Draw",
            variable = self.v1, value = 'F',
            command = self.processRadiobutton)
        
        # Create 'Clear' button to wipe all shapes
        """ Wipe all drawings entirely at one click """
        clearButton = Button(frame1, text = "Clear All",
            command=self.clearCanvas, relief=GROOVE, bg = 'NavajoWhite')
        saveButton = Button(frame1, text = "Save File",
            command = self.onSave, relief=GROOVE, bg = 'LightSalmon')
        
        # Pack all the buttons
        rbRectangle.grid(row = 1, column = 3)
        rbOval.grid(row = 1, column = 4)
        rbLine.grid(row = 1, column = 5)
        rbPoly.grid(row = 1, column = 6)
        rbFree.grid(row = 1, column = 7)
        clearButton.grid(row = 1, column = 8)
        saveButton.grid(row = 1, column = 10)

        # Canvas and mouse events
        """ Create a canvas and bound it to mouse events """
        canvas = Canvas(window, width=800, height=400, cursor = "pencil", bg= "BlanchedAlmond")
        self.canvas = canvas
        self.canvas.pack()
        self.canvas.bind('<ButtonPress-1>', self.onStart)    # left click 
        self.canvas.bind('<B1-Motion>',     self.onGrow)     # drag
        self.canvas.bind('<ButtonPress-3>', self.onMove)     # right click
        self.canvas.bind('<Double-1>',      self.drawPolygon) # execute creating polygon
        self.canvas.bind('<Motion>',        self.mouseMove)     # pass mouse coordinate continuously
        self.canvas.bind('<B3-Motion>',     self.onDrag)        # drags object under the mouse
        self.polygonstatus = False  # to indicate if the user has started to create a polygon
        self.polygonlist = [] # initiate list to record points to create polygon

        # Remember the last drawing
        self.drawn  = None
        self.shape = self.canvas.create_rectangle
        window.mainloop()
        
    def processRadiobutton(self):
        if self.v1.get() == 'R':
            self.shape = self.canvas.create_rectangle
        if self.v1.get() == 'C':
            self.shape = self.canvas.create_oval
        if self.v1.get() == 'L':
            self.shape = self.canvas.create_line
        if self.v1.get() == 'P':
            self.shape = self.canvas.create_polygon
        if self.v1.get() == 'F':
            self.shape = self.canvas.create_line

    # Create a function for clear button
    def clearButton(self):
        canvas = self.canvas
                
    # Remember mouse click's position when Polygon's RB is clicked
    def onStart (self, event):
        if self.v1.get() == "P":
            self.polygonlist.append (event.x)
            self.polygonlist.append (event.y)
            self.polygonstatus = True
        self.start = event
        self.drawn = None

    # Create Polygon and configure the shape
    def drawPolygon (self,event):
        """ Method to cause a 'fill' effect when Polygon is double-clicked"""
        if self.v1.get () == "P" and len(self.polygonlist) > 0:
            objectId = self.shape(self.polygonlist, fill = self.fillColor.get())
            self.drawn = objectId
            self.polygonlist = []
            self.polygonstatus = False
            self.canvas.delete ("polygonLine")
        else:
            pass

    # Polygon's rubber effect
    def mouseMove (self,event):
        """ Lines to create Polygon will have its rubber effect and joined with another lines """
        if self.v1.get() == "P" and self.polygonstatus == True:
            canvas = event.widget
            #   Deletes previous drawing before redrawing again
            if self.drawn:canvas.delete(self.drawn)
            objectId = self.canvas.create_line(self.polygonlist [-2],\
                                               self.polygonlist [-1],\
                                               event.x, event.y, fill = \
                                               self.fillColor.get(),\
                                               tags = "polygonLine")
            self.drawn = objectId
            
    # Elastic drawing: delete and redraw, repeatedly
    def onGrow(self, event):
        """ Move shape on its clicked spot if 'Free Drawing' option is chosen """
        if self.v1.get() == 'F':
            self.onDraw(event)
            return None
        canvas = event.widget
        if self.drawn: canvas.delete(self.drawn)
        objectId = self.shape(self.start.x, self.start.y, event.x, 
            event.y, fill=self.fillColor.get(), width = 0)
        self.drawn = objectId

    # Free Drawing
    """ This method is used for free elastic drawing """
    def onDraw(self,event):
        if self.v1.get() == 'F':
            self.canvas.create_line(self.start.x, self.start.y, event.x, event.y, fill = self.fillColor.get())
            self.start = event
            
    # Move the shape to the click spot
    def onMove(self, event):
            self.start = event

    # Drags object under mouse
    def onDrag(self, event):
        under_mouse = self.canvas.find_withtag(CURRENT)     # To find the object that is currently under the mouse
        diffX, diffY = event.x - self.start.x, event.y - self.start.y # Compute the distance of move method
        self.canvas.move(under_mouse, diffX, diffY)
        self.start = event

    # Create clear canvas method
    def clearCanvas(self):
        self.canvas.delete("all")

    # Saves the file as PS file
    def onSave (self):
        file_format = [('Encapsulated PostScript', '*.eps')]
        filename = asksaveasfilename (defaultextension = '.eps', filetypes = file_format)
        self.canvas.postscript(file = filename)

if __name__ == '__main__': DrawRubberShapes()
