import random
import sys
import string

print("Python Word Cloud")

''' Begin HTML Configuration '''

# wordcloud's box specs and costumization
def make_HTML_box(body):                               
    box_str = """<div style=\"
    width: 560px;
    background-color: #000;
    border: 2px grey solid;
    text-align: center\" >{:s}</div>
    """
    return box_str.format(body)

# create custom settings for each words in wordcloud
def make_HTML_word(word,cnt,high,low):                  
    htmlBig = 96
    htmlLittle = 14
    if high != low:
        ratio = (cnt-low)/float(high-low)
    else:
        ratio = 0
    fontsize = htmlBig*ratio + (1-ratio)*htmlLittle
    fontsize = int(fontsize)
    # randomize word colors with hex digits
    word_str = '''<span style=\"font-size:{:s}px;
               color : ''' + '#' + random.choice(string.hexdigits)+random.choice(string.hexdigits)+random.choice(string.hexdigits)+\
               random.choice(string.hexdigits)+random.choice(string.hexdigits)+random.choice(string.hexdigits)+'''\">{:s}</span>'''
    return word_str.format(str(fontsize), word)

def print_HTML_file(body,title):
    # construct word cloud in HTML file
    fd = open(title+'.html','w')
    the_str="""
    <html> <head>
    <title>"""+title+"""</title>
    </head>

    <body>
    <h1>"""+title+'</h1>'+'\n'+body+'\n'+"""<hr>
    </body> </html>
    """
    fd.write(the_str)
    fd.close()

''' End HTML section '''

''' Constructing word cloud program using lists '''
# create empty lists
otherList = []                      
list_no = []
sorted_list = []

# prompt user to input data source
while True:
    filename = input("Input designated file (.txt only) : ")
    try:
        openFile = open(filename,"r")
        break
    except IOError:                      
        print("File not found. Re-prompt designated file (.txt only) :")

# open stopWords.txt
while True:
    try:
        stop_words = open("stopWords.txt")
        break
    except IOError:
        # if stopwords.txt is missing, the system will close immidiately
        print('stopWords.txt is missing.')
        sys.exit()

# change variable to its own list 
stopWord = stop_words.read().split()
newList = openFile.read().split()
newList = [word2.strip(string.digits).strip(string.punctuation).lower() for word2 in newList]

# making loops for the new list 
for word in newList:
    if word == "" :
        continue
    if word not in otherList:
        if word not in stopWord:
            otherList.append(word)

# making loops for the new list
for word in otherList:
    count = 0
    for i in newList:
        if i == word:
            count += 1
    list_no.append([count,word])

# sorting and splitting words
list_no.sort(reverse=True)
list_no = list_no[:50]
row = 0

print(filename, ':')
print("{} words in frequency order as <count : word> pairs".format(len(list_no)))

# splitting words, configure distance
for (count,word) in list_no :
    if row%4 == 0:
        print()
    print("{:>3} => {:<13}".format(count,word), end='')
    row += 1

# reverse the old list with the new empty list
for (count, word) in list_no :
    sorted_list.append([word, count])
sorted_list.sort(reverse=False)
high_count = list_no[0][0]
low_count = list_no[-1][0]
body = ""

# insert HTML function to the wordcloud.py
for (word,count) in sorted_list:
    body = body + " " + make_HTML_word(word,count,high_count,low_count)
box = make_HTML_box(body)
print_HTML_file(box,"A word cloud of {}".format(filename))

print()
input("\nPress enter to exit...")

stop_words.close()
openFile.close()
    
######################### ######      END     ###### #########################
######################### ######    PROGRAM   ###### #########################

