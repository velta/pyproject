# Final Prep - No 2

""" Find the output generated for input :
We bought 3 shirts and 2 pairs of shoes for 99 dollars. """

def main():
    line = input("Enter a line of text: ")
    wordList = line.split()
    ints = []
    words = []
    for element in wordList:
        try:
            val = int(element)
            ints.append(val)
        except ValueError:
            words.append(element)

    print("({},{})".format(ints,words))

main()

""" Result :
([3, 2, 99],['We', 'bought', 'shirts', 'and',
'pairs', 'of', 'shoes', 'for', 'dollars.'])
