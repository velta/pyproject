# Final Prep - No 5

""" Background color can be changed if user pressed
r-key, g-key, or b-key on the keyboard.
(R = red, G = green, B = blue)
The previous canvas is white. """

from tkinter import *

class RGBCanvas(object):
    def __init__(self):
        window = Tk()                       # Create a window
        window.title("Red, Green, or Blue") # Set a title
        self.canvas = Canvas(window, bg = "white",
                             width = 300, height = 300)
        self.canvas.pack()
        self.canvas.focus_set()             # Set the input focus to canvas

        self.canvas.bind("r", self.goRed)
        self.canvas.bind("g", self.goGreen)
        self.canvas.bind("b", self.goBlue)

        window.mainloop()

    def goRed(self,event): self.canvas["bg"] = "red"
    def goGreen(self,event): self.canvas["bg"] = "green"
    def goBlue(self,event): self.canvas["bg"] = "blue"

RGBCanvas()
