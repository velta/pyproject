# Final Prep - No 1

""" Find generated output of these methods below """

def swap1(param1, param2):
	print("Before swap :", param1, param2)
	param1, param2 = param2, param1
	print("After swap :", param1, param2)

def swap2(param, i, k):
	param[i], param[k] = param[k], param[i]

def main():
	num = [5,7,9]
	swap1(num[1], num[2])
	print("After swap1, num:", num)
	swap2(num, 1, 2)
	print("After swap2, num:", num)
main()

""" Output Generated =
Before: 7 9
After: 9 7
After swap1, num: [5, 7, 9]
After swap2, num: [5, 9, 7] """
